@include('inc.header')

    <div class="container">
        <div class="row">
        @if(session('info'))
        <div class="alert alert-success">
            {{session('info')}}
        </div>
        @endif
        <table class="table table-hover">
        <thead>
            <tr class="table-success">
            <th scope="col">ID</th>
            <th scope="col">Nama</th>
            <th scope="col">Alamat</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        @if(count($namaorangs)>0)
            @foreach($namaorangs->all() as $namaorang)
            <tr class="table-dark">
            <th scope="row">{{ $namaorang->id }}</th>
            <td>{{ $namaorang->nama }}</td>
            <td>{{ $namaorang->alamat }}</td>
            <td>
                <a href='{{ url("/read/{$namaorang->id}") }}' class="btn btn-success">Read </a> |
                <a href='{{ url("/update/{$namaorang->id}") }}' class="btn btn-success">Update </a> |
                <a href='{{ url("/delete/{$namaorang->id}") }}' class="btn btn-danger">Delete </a> 
            </td>
            </tr>
            @endforeach
        @endif
        </tbody>
        </table> 
        </div>
    </div>

@include('inc.footer')