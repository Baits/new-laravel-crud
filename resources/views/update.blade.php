@include('inc.header')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{url('/edit',array($namaorangs->id))}}">
            {{csrf_field()}}
            <fieldset>
                <legend>Laravel CRUD BOOTSTRAP</legend>
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{$error}}
                        </div>
                    @endforeach
                @endif
                <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" name="nama" id="exampleInputEmail1" value="<?php echo $namaorangs->nama ?>" placeholder="Masukan Nama">
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">Alamat</label>
                <textarea class="form-control" name="alamat" cols="50px" rows="10px" id="exampleInputPassword1" placeholder="Alamat"><?php echo $namaorangs->alamat ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{url('/')}}" class ="btn btn-primary">Back</a>
            </fieldset>
            </form>
            </div>
        </div>
    </div>
@include('inc.footer')