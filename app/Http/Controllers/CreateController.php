<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\namaorang;

use App\Http\Requests;

class CreateController extends Controller
{
    public function home(){
        $namaorangs = namaorang::all();
        return view('home', ['namaorangs'=>$namaorangs]);
    }

    public function create(){
        
        return view('create');
    }

    public function read($id){
        $namaorangs = namaorang::find($id);

        return view('read')
        ->with('namaorangs', $namaorangs);
    }

    public function add(Request $request){
        $this->validate($request, [
            'nama'=>'required',
            'alamat'=>'required'
        ]);
        $namaorangs = new namaorang;
        $namaorangs->nama = $request ->input('nama');
        $namaorangs->alamat = $request ->input('alamat');
        $namaorangs->save();
        return redirect('/')->with('info', 'Data Berhasil Disimpan!');
    }

    public function update($id){
        $namaorangs = namaorang::find($id);
        return view('update', ['namaorangs'=>$namaorangs]);
    }

    public function edit(Request $request, $id){
        $this->validate($request, [
            'nama'=>'required',
            'alamat'=>'required'
        ]);
        $data = array(
            'nama'=>$request->input('nama'),
            'alamat'=>$request->input('alamat'),
        );
        namaorang::where('id', $id)
        ->update($data);
        return redirect('/')->with('info', 'Data Berhasil Diubah!');
    }

    public function delete($id){
        namaorang::where('id', $id)
        ->delete();
        return redirect('/')->with('info', 'Data Berhasil Dihapus!');
    }
}
