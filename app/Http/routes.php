<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'CreateController@home');
Route::get('/create', 'CreateController@create');
Route::get('/read/{id}', 'CreateController@read');
Route::post('/insert', 'CreateController@add');
Route::get('/update/{id}', 'CreateController@update');
Route::post('/edit/{id}', 'CreateController@edit');
Route::get('/delete/{id}', 'CreateController@delete');